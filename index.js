require("dotenv").config();
const connectDB = require("./connectDB");
const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const cors = require("cors");
const session = require("express-session");
const userRoutes = require("./routes/userRoutes");
const appointmentRoutes = require("./routes/appointmentRoutes");
const viewRoutes = require("./routes/viewRoutes");
const app = express();
const store = new session.MemoryStore();

connectDB();

//Middlewares:
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(session({ secret: process.env.SESSION_SECRET, cookie: { maxAge: 18000000, sameSite: "lax" }, resave: false, saveUninitialized: false, store }));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());

app.use(express.static(path.join(__dirname, "views")));

app.use("/api/user", userRoutes);
app.use("/api/appointment", appointmentRoutes);
app.use("/", viewRoutes);

// Start the server
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
