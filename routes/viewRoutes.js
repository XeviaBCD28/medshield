const express = require("express");
const router = express.Router();
const viewsController = require("../controllers/viewController");
const { isAuthenticatedAsUser, isAuthenticatedAsAdmin, isAuthenticatedAsDoctor } = require("../controllers/userController");

router.get("/", viewsController.getHomePage);
router.get("/login", viewsController.getLoginPage);
router.get("/signup", viewsController.getSignupPage);

router.get("/userpage", isAuthenticatedAsUser, viewsController.getUserHomePage);
router.get("/userpanel", isAuthenticatedAsUser, viewsController.getUserPanelPage);

router.get("/doctorpage", isAuthenticatedAsDoctor, viewsController.getDoctorHomePage);
router.get("/doctorpanel", isAuthenticatedAsDoctor, viewsController.getDoctorPanelPage);

router.get("/adminpage", isAuthenticatedAsAdmin, viewsController.getAdminHomePage);

module.exports = router;