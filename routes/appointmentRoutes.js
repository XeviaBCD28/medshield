const express = require("express")
const AppointmentController = require("../controllers/appointmentController")
const router = express.Router();

router.post("/", AppointmentController.createAppointment);
router.get("/", AppointmentController.getAllAppointments);
router.put("/changeStatus/:id", AppointmentController.changeAppointmentStatus);
router.get('/available-slots/:doctorId/:date', AppointmentController.getAvailableAppointments);
router
    .route("/:id")
    .get(AppointmentController.getAppointmentById)
    .put(AppointmentController.updateAppointment)
    .delete(AppointmentController.deleteAppointment);

module.exports = router;