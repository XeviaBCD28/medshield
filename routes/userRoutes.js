const express = require("express")
const UserController = require("../controllers/userController")
const router = express.Router();

router.post("/login", UserController.login);
router.post("/signup", UserController.createUser);
router.post("/verifyOTP", UserController.verifyOTP);
router.get("/logout", UserController.logout);
router.get("/", UserController.getAllUsers);
router.get("/getData", UserController.getSessionData);
router.put("/addAppointment/:userId/:appointmentId", UserController.addAppointmentToUser);
router.put("/changeStatus/:id", UserController.changeUserStatus);
router
    .route("/:id")
    .get(UserController.getUserById)
    .put(UserController.uploadUserPhoto, UserController.updateUser)
    .delete(UserController.deleteUser);

module.exports = router;