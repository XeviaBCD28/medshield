const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const validator = require("validator")

const userSchema = new mongoose.Schema(
    {
        name: { type: String, required: true },
        email: { type: String, required: true, unique: true, lowercase: true, validate: validator.isEmail },
        password: { type: String, required: true, select: false, minlength: 8 },
        role: { type: String, enum: ["user", "doctor", "admin"], default: "user" },
        is_active: { type: Boolean, default: true },
        photo: { type: String, default: "views\\images\\profile.png" },
        specialization: {
            type: String,
            required: function () {
                return this.role === 'doctor';
            }
        },
        appointments: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Appointment',
            validate: {
                validator: async function (v) {
                    const project = await mongoose.model('Appointment').findById(v);
                    return project !== null;
                },
                message: props => `${props.value} is not a valid appointment ID`
            }
        }]

    }
);

userSchema.pre("save", async function (next) {
    if (!this.isModified("password")) return next(); // If password is not modified, skip hashing
    this.password = await bcrypt.hash(this.password, 12);
    next();
});

userSchema.pre("findOneAndUpdate", async function (next) {
    if (!this._update.password) return next(); // If password is not modified, skip hashing
    try {
        const hashedPassword = await bcrypt.hash(this._update.password, 12);
        this._update.password = hashedPassword; // Replace plain password with hashed password
        next();
    } catch (error) {
        next(error);
    }
});

userSchema.methods.checkPasswordMatch = async function (candidatePassword, userPassword) {
    return await bcrypt.compare(candidatePassword, userPassword)
};

const User = mongoose.model("User", userSchema);

module.exports = User;
