const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf, errors } = format;

// Custom log format
const logFormat = printf(({ level, message, label, timestamp, stack }) => {
    return `${timestamp} [${label}] ${level}: ${stack || message}`;
});

const logger = createLogger({
    level: 'info',
    format: combine(
        label({ label: 'MedShield' }),
        timestamp(),
        errors({ stack: true }),
        logFormat
    ),
    transports: [
        new transports.File({ filename: 'logs/error.log', level: 'error' }),
        new transports.File({ filename: 'logs/combined.log' })
    ]
});

module.exports = logger;