const Appointment = require("../models/appointmentModel");
const User = require('../models/userModel');
const nodemailer = require('nodemailer');
const logger = require('./logger');

const transporter = nodemailer.createTransport({
    service: process.env.EMAIL_SERVICE,
    auth: {
        user: process.env.EMAIL_USER,
        pass: process.env.EMAIL_PASS
    }
});

const sendAppointmentConfirmationEmail = async (user, appointmentDetails) => {
    const message = `
        Dear ${user.name},

        This is to confirm that your appointment has been successfully booked.

        Appointment Details:
        Date: ${appointmentDetails.date}
        Time: ${appointmentDetails.time}
        Doctor: ${appointmentDetails.doctorName}
        Specialization: ${appointmentDetails.specialization}

        If you have any questions or need to reschedule, please contact us at support@medshield.com.

        Best regards,
        The MedShield Team
    `;
    const mailOptions = {
        from: process.env.EMAIL_USER,
        to: user.email,
        subject: 'Appointment Confirmation - MedShield',
        text: message
    };
    await transporter.sendMail(mailOptions);
    logger.info('Appointment confirmation email sent', { userId: user._id, appointmentId: appointmentDetails.appointmentId });
};

exports.getAllAppointments = async (req, res) => {
    try {
        const appointments = await Appointment.find();
        logger.info('Fetched all appointments');
        res.json({ data: appointments, status: "success" });
    } catch (err) {
        logger.error('Error fetching all appointments', { error: err.message });
        res.status(500).json({ error: err.message });
    }
};

exports.createAppointment = async (req, res) => {
    try {
        const existingAppointment = await Appointment.findOne({ dateTime: req.body.dateTime });
        if (existingAppointment) {
            logger.warn('Attempt to create duplicate appointment', { userId: req.body.user, dateTime: req.body.dateTime });
            return res.status(400).json({ error: 'Appointment with this date and time already exists' });
        }
        const newAppointment = await Appointment.create(req.body);
        const user = await User.findById(req.body.user);
        const doctor = await User.findById(req.body.doctor);
        const appointmentDetails = {
            date: newAppointment.dateTime.toDateString(),
            time: newAppointment.dateTime.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' }),
            doctorName: doctor.name,
            specialization: doctor.specialization,
            appointmentId: newAppointment._id
        };
        await sendAppointmentConfirmationEmail(user, appointmentDetails);
        logger.info('Appointment created successfully', { appointmentId: newAppointment._id, userId: req.body.user });
        res.status(201).json({ data: newAppointment, status: "success" });
    } catch (err) {
        logger.error('Error creating appointment', { error: err.message });
        res.status(500).json({ error: err.message });
    }
};

exports.getAppointmentById = async (req, res) => {
    try {
        const appointment = await Appointment.findById(req.params.id);
        if (!appointment) {
            logger.warn('Appointment not found', { appointmentId: req.params.id });
            return res.status(404).json({ error: 'Appointment not found' });
        }
        logger.info('Fetched appointment by ID', { appointmentId: req.params.id });
        res.json({ data: appointment, status: "success" });
    } catch (err) {
        logger.error('Error fetching appointment by ID', { error: err.message });
        res.status(500).json({ error: err.message });
    }
};

exports.updateAppointment = async (req, res) => {
    try {
        const existingAppointment = await Appointment.findOne({ dateTime: req.body.dateTime });
        if (existingAppointment) {
            logger.warn('Attempt to update to a duplicate appointment time', { userId: req.body.user, dateTime: req.body.dateTime });
            return res.status(400).json({ error: 'Appointment with this date and time already exists' });
        }
        const updatedAppointment = await Appointment.findByIdAndUpdate(req.params.id, req.body, { new: true, runValidators: true });
        if (!updatedAppointment) {
            logger.warn('Appointment to update not found', { appointmentId: req.params.id });
            return res.status(404).json({ error: 'Appointment not found' });
        }
        req.session.appointment = updatedAppointment;
        logger.info('Appointment updated successfully', { appointmentId: req.params.id });
        res.json({ data: updatedAppointment, status: "success" });
    } catch (err) {
        logger.error('Error updating appointment', { error: err.message });
        res.status(500).json({ error: err.message });
    }
};

exports.changeAppointmentStatus = async (req, res) => {
    try {
        const updatedAppointment = await Appointment.findByIdAndUpdate(req.params.id, { status: req.body.status }, { new: true, runValidators: true });
        if (!updatedAppointment) {
            logger.warn('Appointment to change status not found', { appointmentId: req.params.id });
            return res.status(404).json({ error: 'Appointment not found' });
        }
        logger.info('Appointment status updated successfully', { appointmentId: req.params.id, status: req.body.status });
        res.json({ data: updatedAppointment, status: "success" });
    } catch (err) {
        logger.error('Error changing appointment status', { error: err.message });
        res.status(500).json({ error: err.message });
    }
};

exports.deleteAppointment = async (req, res) => {
    try {
        const appointment = await Appointment.findByIdAndDelete(req.params.id);
        if (!appointment) {
            logger.warn('Appointment to delete not found', { appointmentId: req.params.id });
            return res.status(404).json({ error: 'Appointment not found' });
        }
        logger.info('Appointment deleted successfully', { appointmentId: req.params.id });
        res.json({ message: `${appointment._id} deleted successfully`, status: "success" });
    } catch (err) {
        logger.error('Error deleting appointment', { error: err.message });
        res.status(500).json({ error: err.message });
    }
};

exports.getAvailableAppointments = async (req, res) => {
    const doctorId = req.params.doctorId;
    const date = new Date(req.params.date);
    try {
        const availableSlots = await getAvailableSlots(doctorId, date);
        logger.info('Fetched available slots', { doctorId: doctorId, date: date.toISOString() });
        res.json(availableSlots);
    } catch (error) {
        logger.error('Error fetching available slots', { error: error.message });
        res.status(500).json({ error: 'Internal Server Error' });
    }
};

const getAvailableSlots = async (doctorId, date) => {
    const slotDuration = 15 * 60 * 1000; // 15 minutes
    const clonedDate = new Date(date);
    const periods = [
        { startTime: new Date(clonedDate.setHours(9, 0, 0, 0)), endTime: new Date(clonedDate.setHours(13, 0, 0, 0)) }, // 9AM - 1PM
        { startTime: new Date(clonedDate.setHours(14, 0, 0, 0)), endTime: new Date(clonedDate.setHours(17, 0, 0, 0)) } // 2PM - 5PM
    ];
    const allSlots = generateTimeSlots(periods, slotDuration);
    const availableSlots = await Promise.all(allSlots.map(async (slot) => {
        const isSlotTaken = await checkTakenSlot(doctorId, slot);
        return isSlotTaken ? null : slot;
    }));
    return availableSlots.filter(slot => slot !== null);
};

const generateTimeSlots = (periods, slotDuration) => {
    let slots = [];
    periods.forEach(period => {
        let currentTime = new Date(period.startTime);
        while (currentTime < period.endTime) {
            slots.push(new Date(currentTime));
            currentTime = new Date(currentTime.getTime() + slotDuration);
        }
    });
    return slots;
};

const checkTakenSlot = async (doctorId, appointmentDateTime) => {
    const appointments = await Appointment.find({
        doctor: doctorId,
        dateTime: appointmentDateTime
    });
    let taken = appointments.length === 0 ? false : true;
    return taken;
};