const path = require("path");

exports.getLoginPage = (req, res) => res.sendFile(path.join(__dirname, "../", "views", "login.html"));
exports.getSignupPage = (req, res) => res.sendFile(path.join(__dirname, "../", "views", "signup.html"));
exports.getHomePage = (req, res) => res.sendFile(path.join(__dirname, "../", "views", "index.html"));
exports.getUserHomePage = (req, res) => res.sendFile(path.join(__dirname, "../", "views", "userpage.html"));
exports.getDoctorHomePage = (req, res) => res.sendFile(path.join(__dirname, "../", "views", "doctorpage.html"));
exports.getUserPanelPage = (req, res) => res.sendFile(path.join(__dirname, "../", "views", "userpanel.html"));
exports.getDoctorPanelPage = (req, res) => res.sendFile(path.join(__dirname, "../", "views", "doctorpanel.html"));
exports.getAdminHomePage = (req, res) => res.sendFile(path.join(__dirname, "../", "views", "adminpage.html"));
