const User = require("../models/userModel");
const multer = require("multer");
const nodemailer = require("nodemailer");
const logger = require('./logger');
const bcrypt = require("bcryptjs");
const UserOTP = require("../models/userOTPModel");
const validator = require('validator');

const sanitizeUserInput = (input) => {
    if (typeof input === 'string') {
        return validator.escape(input);
    }
    return input;
};

const transporter = nodemailer.createTransport({
    service: process.env.EMAIL_SERVICE,
    auth: {
        user: process.env.EMAIL_USER,
        pass: process.env.EMAIL_PASS
    }
});

const sendSignupEmail = async (name, email, role, password) => {
    let message = `
        Dear ${name},

        Welcome to MedShield, your trusted partner in secure healthcare management.

        We are thrilled to have you on board. Your account has been successfully created and you can now access our comprehensive healthcare management services. 

        ${role === 'doctor' ? `As a doctor, you will have access to advanced features tailored to your needs. Here are your login details:

        Email: ${email}
        Password: ${password}` : ''}

        Please make sure to keep your account details secure and do not share your password with anyone.

        If you have any questions or need assistance, our support team is here to help. You can reach us at support@medshield.com.

        Thank you for choosing MedShield.

        Best regards,
        The MedShield Team
    `;
    if (role === 'doctor') {
        message += `\n\nYour login details:\nEmail: ${email}\nPassword: ${password}`;
    }
    const mailOptions = {
        from: process.env.EMAIL_USER,
        to: email,
        subject: 'Account Created',
        text: message
    };
    await transporter.sendMail(mailOptions);
    logger.info('User acount creation email sent', { userName: name, userEmail: email });
};

const sendOTPMail = async (id, email) => {
    try {
        const otp = `${Math.floor(1000 + Math.random() * 9000)}`;
        const mailOptions = {
            from: process.env.EMAIL_USER,
            to: email,
            subject: "OTP verification for logging in to MedShield",
            html: `<p>Enter <b>${otp}</b> in the website to verify your email address.</p><p>This code expires in <b>5 minutes</b></p>`
        };
        const hashedOTP = await bcrypt.hash(otp, 10);
        const OTPVerification = await new UserOTP({
            user: id,
            otp: hashedOTP,
            createdAt: Date.now(),
            expiresAt: Date.now() + 300000
        });
        await OTPVerification.save();
        await transporter.sendMail(mailOptions);
        logger.info('User login otp email sent.', { userEmail: email });
    } catch (err) {
        logger.error('Error sending otp mail', { error: err.message });
        res.status(500).json({ error: err.message });
    }
}

exports.isAuthenticatedAsUser = (req, res, next) => {
    if (req.session.authenticated && req.session.user.role === 'user') {
        return next();
    } else if (req.session.authenticated) {
        return res.status(401).json({ error: "Unauthorized" })
    }
    res.redirect('/login');
};

exports.isAuthenticatedAsDoctor = (req, res, next) => {
    if (req.session.authenticated && req.session.user.role === 'doctor') {
        return next();
    } else if (req.session.authenticated) {
        return res.status(401).json({ error: "Unauthorized" })
    }
    res.redirect('/login');
};

exports.isAuthenticatedAsAdmin = (req, res, next) => {
    if (req.session.authenticated && req.session.user.role === 'admin') {
        return next();
    } else if (req.session.authenticated) {
        return res.status(403).json({ error: "Unauthorized" });
    }
    res.redirect('/login');
};

exports.getAllUsers = async (req, res) => {
    try {
        const users = await User.find();
        logger.info('Fetched all users');
        res.json({ data: users, status: "success" });
    } catch (err) {
        logger.error('Error fetching all users', { error: err.message });
        res.status(500).json({ error: err.message });
    }
};

exports.createUser = async (req, res) => {
    try {
        const existingUser = await User.findOne({ email: req.body.email });
        const sanitizedEmail = validator.normalizeEmail(req.body.email);
        const sanitizedName = sanitizeUserInput(req.body.name);
        const sanitizedRole = sanitizeUserInput(req.body.role);
        const sanitizedPassword = sanitizeUserInput(req.body.password);
        if (existingUser) {
            logger.warn('Attempt to create duplicate email', { email: sanitizedEmail });
            return res.status(400).json({ error: 'User with this email already exists' });
        }
        const newUser = await User.create(req.body);
        await sendSignupEmail(sanitizedName, sanitizedEmail, sanitizedRole, sanitizedPassword);
        logger.info('User created successfully', { userName: sanitizedName, userEmail: sanitizedEmail });
        res.status(201).json({ data: newUser, status: "success" });
    } catch (err) {
        logger.error('Error creating user', { error: err.message });
        res.status(500).json({ error: err.message });
    }
};

exports.getUserById = async (req, res) => {
    try {
        const user = await User.findById(req.params.id);
        logger.info('Fetched user', { id: req.params.id });
        res.json({ data: user, status: "success" })
    } catch (err) {
        logger.error('Error fetching user', { error: err.message });
        res.status(500).json({ error: err.message });
    }
};

exports.updateUser = async (req, res) => {
    try {
        const filteredReqBody = filterObj(req.body, "name", "email", "password", "role", "is_active");
        if (req.file) filteredReqBody.photo = req.file.path;

        const existingUser = await User.findOne({ email: req.body.email });
        if (existingUser) {
            logger.warn('Attempt to update with duplicate email', { email: req.body.email });
            return res.status(400).json({ error: 'User with this email already exists' });
        }

        const updatedUser = await User.findByIdAndUpdate(req.params.id, filteredReqBody, { new: true, runValidators: true });
        req.session.user = updatedUser;

        logger.info('User updated successfully', { userId: req.params.id, updatedUserData: filteredReqBody });
        res.json({ data: updatedUser, status: "success" });
    } catch (err) {
        logger.error('Error updating user', { error: err.message });
        res.status(500).json({ error: err.message })
    }
};

exports.changeUserStatus = async (req, res) => {
    try {
        const updatedUser = await User.findByIdAndUpdate(req.params.id, { is_active: req.body.is_active }, { new: true, runValidators: true });
        logger.info('User status changed successfully', { userId: req.params.id, newStatus: req.body.is_active });
        res.json({ data: updatedUser, status: "success" });
    } catch (err) {
        logger.error('Error changing user status', { error: err.message });
        res.status(500).json({ error: err.message });
    }
};

exports.deleteUser = async (req, res) => {
    try {
        const user = await User.findByIdAndDelete(req.params.id);
        logger.info('User deleted successfully', { userId: user._id });
        res.json({ message: `${user._id} deleted successfully`, status: "success" });
    } catch (err) {
        logger.error('Error deleting user', { error: err.message });
        res.status(500).json({ error: err.message });
    }
};

exports.login = async (req, res) => {
    try {
        const { email, password } = req.body;

        const sanitizedEmail = sanitizeUserInput(email);
        const sanitizedPassword = sanitizeUserInput(password);

        if (!validator.isEmail(sanitizedEmail) || !sanitizedPassword) {
            logger.error('Invalid login request: Missing or invalid email or password');
            return res.status(400).json({ error: "Please provide a valid email and password!" });
        }

        const user = await User.findOne({ email: sanitizedEmail }).select("+password");
        if (!user) {
            logger.warn('User not found during login attempt', { email: sanitizedEmail });
            return res.status(404).json({ error: "User with the specified email not registered" });
        }

        if (!user.is_active) {
            logger.warn('Inactive user attempted to login', { email: sanitizedEmail });
            return res.status(403).json({ error: "User not activated!" });
        }

        const passwordMatch = await bcrypt.compare(sanitizedPassword, user.password);
        if (!passwordMatch) {
            logger.warn('Incorrect password during login attempt', { email: sanitizedEmail });
            return res.status(401).json({ error: "Incorrect password" });
        }

        sendOTPMail(user._id, sanitizedEmail);
        req.session.authenticated = true;
        req.session.user = user;
        logger.info('User logged in successfully', { email: sanitizedEmail });
        res.json(req.session);
    } catch (err) {
        logger.error('Error during login', { error: err.message });
        res.status(500).json({ error: err.message });
    }
};


exports.logout = (req, res) => {
    try {
        if (!req.session.authenticated) {
            logger.warn('Attempt to logout without an active session');
            return res.json({ message: "No active session to logout from" });
        }

        req.session.destroy((err) => {
            if (err) {
                logger.error('Failed to logout', { error: err.message });
                return res.status(500).json({ error: "Failed to logout" });
            }
            logger.info('Logout successful');
            res.json({ message: "Logout successful" });
        });
    } catch (err) {
        logger.error('Error during logout', { error: err.message });
        res.status(500).json({ error: err.message });
    }
};

exports.addAppointmentToUser = async (req, res) => {
    try {
        await User.findByIdAndUpdate(
            req.params.userId,
            { $push: { appointments: req.params.appointmentId } },
            { new: true, useFindAndModify: false }
        );
        const updatedUser = await User.findById(req.params.userId);
        req.session.user = updatedUser;

        logger.info('Appointment added to user successfully', { userId: req.params.userId, appointmentId: req.params.appointmentId });
        res.json({ message: 'Appointment added to user successfully.' });
    } catch (err) {
        logger.error('Error adding appointment to user', { error: err.message });
        res.status(500).json({ error: err.message });
    }
};

exports.getSessionData = (req, res) => {
    try {
        const user = req.session.user;
        res.status(200).json(user);
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
};

exports.verifyOTP = async (req, res) => {
    try {
        let { user, otp } = req.body;
        if (!user || !otp) throw Error("Empty otp details not allowed!");
        const userOTP = await UserOTP.find({ user });
        if (userOTP.length <= 0) throw new Error("Account record does not exist or has been verified already!");
        const { expiresAt } = userOTP[0].expiresAt;
        const hashedOTP = userOTP[0].otp;
        if (expiresAt < Date.now()) {
            await UserOTP.deleteMany({ user });
            throw new Error("Code has expired. Please login again!");
        } else {
            const valid = await bcrypt.compare(otp, hashedOTP);
            if (!valid) throw new Error("Invalid OTP. Check your email!")
            await UserOTP.deleteMany({ user });
            logger.info('OTP verified successfully');
            res.json({ message: "OTP verified successfully" });
        }
    } catch (err) {
        logger.error('Error while verifying OTP', { error: err.message });
        res.status(500).json({ error: err.message });
    }
}

const storageEngine = multer.diskStorage({
    destination: (req, file, cb) => cb(null, "views/images"),
    filename: (req, file, cb) => {
        const ext = file.mimetype.split("/")[1];
        cb(null, `user-${Date.now()}.${ext}`);
    }
});

const filterObj = (obj, ...allowedFields) => {
    const newObj = {};
    Object.keys(obj).forEach((el) => {
        if (allowedFields.includes(el)) newObj[el] = obj[el]
    });
    return newObj;
}

const multerFilter = (req, file, cb) => {
    if (file.mimetype.startsWith("image")) {
        cb(null, true);
    } else {
        cb(new AppError("Not an image! Please upload only images", 400), false);
    }
};

const upload = multer({
    storage: storageEngine,
    fileFilter: multerFilter
});

exports.uploadUserPhoto = upload.single("photo");