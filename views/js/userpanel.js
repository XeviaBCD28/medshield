const btn = document.querySelector(".mybtn");
const bookAppointment = document.querySelector(".bookAppointment");
const timeSlots = document.getElementById("time");
const fullname = document.getElementById("fullname");
const userEmail = document.getElementById("email");
const userPic = document.querySelector(".profilepic");
const appointmentDate = document.getElementById("appointmentDate");
const specializationSelected = document.getElementById("specialization");
const doctorSelected = document.getElementById("doctor");
const dateSelected = document.getElementById("appointmentDate");
const timeSelected = document.getElementById("time");
const tbody = document.querySelector(".tablebody");

let loggedInUser;
let doctors = [];
let userProfilePicFile;

window.addEventListener('load', async function () {
    try {
        // Fetch user data
        let userResponse = await axios.get('/api/user/getData');
        let user = userResponse.data;
        loggedInUser = user;
        fullname.placeholder = escapeHTML(user.name);
        userEmail.placeholder = escapeHTML(user.email);
        userPic.src = escapeHTML(user.photo.split("views\\")[1]);

        tbody.innerHTML = "";
        user.appointments.forEach(async (appointmentId) => {
            let appointmentRes = await axios.get(`/api/appointment/${appointmentId}`);
            let appointment = appointmentRes.data.data;
            let doctorRes = await axios.get(`/api/user/${appointment.doctor}`);
            let doctor = doctorRes.data.data;
            let { date, time } = convertISOToDateTime(appointment.dateTime);

            const row = document.createElement('tr');
            const doctorTd = document.createElement("td");
            doctorTd.textContent = escapeHTML(doctor.name);
            const dateTd = document.createElement("td");
            dateTd.textContent = escapeHTML(date);
            const timeTd = document.createElement("td");
            timeTd.textContent = escapeHTML(time);
            const statusTd = document.createElement("td");
            statusTd.textContent = escapeHTML(appointment.status.charAt(0).toUpperCase() + appointment.status.slice(1));

            row.appendChild(doctorTd);
            row.appendChild(dateTd);
            row.appendChild(timeTd);
            row.appendChild(statusTd);
            tbody.appendChild(row);
        })

        // Fetch users data
        let usersResponse = await axios.get('/api/user');
        let usersData = usersResponse.data.data;
        doctors = usersData.filter(user => user.role === "doctor");

        const doctorSelect = document.getElementById("doctor");
        doctors.forEach(doctor => {
            const option = document.createElement('option');
            option.value = doctor._id;
            option.textContent = doctor.name;
            doctorSelect.appendChild(option);
        });
        setMinDate();
        // Ensure at least one doctor is available
        if (doctors.length > 0) {
            // let lastDoctorId = doctors[doctors.length - 1]._id;

            // Get today's date
            let today = new Date();
            function formatDate(date) {
                const year = date.getFullYear();
                const month = String(date.getMonth() + 1).padStart(2, '0');
                const day = String(date.getDate()).padStart(2, '0');
                return `${year}-${month}-${day}`;
            }
            let formattedToday = formatDate(today);
            appointmentDate.value = formattedToday;
            // Fetch available slots for the last doctor
            await fetchAndUpdateSlots();
        }

        document.getElementById('js-preloader').classList.add('loaded');

    } catch (error) {
        console.error(error);
        alert("An error occurred while fetching data.");
        document.getElementById('js-preloader').classList.add('loaded');
    }
});

document.querySelector(".changepic").addEventListener("change", (e) => {
    if (e.target.files && e.target.files[0]) {
        document.querySelector(".profilepic").src = URL.createObjectURL(e.target.files[0]);
        userProfilePicFile = e.target.files[0];
        document.querySelector(".rmphoto").style.display = "inline";
    }
});

document.querySelector(".rmphoto").addEventListener("click", () => {
    document.querySelector(".rmphoto").style.display = "none";
    document.querySelector(".profilepic").src = user.photo.split("views\\")[1];
    userProfilePicFile = null;
});

const setMinDate = () => {
    const today = new Date();
    const year = today.getFullYear();
    const month = String(today.getMonth() + 1).padStart(2, '0');
    const day = String(today.getDate()).padStart(2, '0');
    const minDate = `${year}-${month}-${day}`;
    document.getElementById('appointmentDate').setAttribute('min', minDate);
};

function convertISOToDateTime(isoString) {
    const date = new Date(isoString);
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0'); // Months are zero-indexed
    const day = String(date.getDate()).padStart(2, '0');
    const dateString = `${year}-${month}-${day}`;
    let hours = date.getHours();
    const minutes = String(date.getMinutes()).padStart(2, '0');
    const seconds = String(date.getSeconds()).padStart(2, '0');
    const ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // The hour '0' should be '12'
    const timeString = `${hours}:${minutes} ${ampm}`;
    return {
        date: dateString,
        time: timeString
    };
};

function convertISOToReadable(isoString) {
    const date = new Date(isoString);
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0'); // Months are zero-indexed
    const day = String(date.getDate()).padStart(2, '0');
    let hours = date.getHours();
    const minutes = String(date.getMinutes()).padStart(2, '0');
    const ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // The hour '0' should be '12'
    const dateString = `${month}-${day}-${year}`;
    const timeString = `${hours}:${minutes} ${ampm}`;
    return `${timeString} on ${dateString}`;
};

btn.addEventListener("click", (e) => {
    e.preventDefault();
    let name = fullname.value.trim();
    let email = userEmail.value.trim();
    let password = document.getElementById("password").value.trim();
    let confirmpass = document.getElementById("confirmpassword").value.trim();
    const validEmailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;
    const strongPasswordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;

    if (!name || !email || !password || !confirmpass) return alert("Please fill all the fields before proceeding!");
    if (!validEmailRegex.test(email)) return alert("Please provide a valid email address!");
    if (!strongPasswordRegex.test(password)) return alert("Please provide a strong password! The password must container atleast one lowercase letter, one uppercase letter, one digit, one special character and a minimum of 8 characters.");
    if (password != confirmpass) return alert("Passwords do not match!");

    document.getElementById('js-preloader').classList.remove('loaded');

    const formData = new FormData();
    formData.append("name", name);
    formData.append("password", password);
    if (email.toLowerCase() !== loggedInUser.email) {
        formData.append("email", email);
    }
    if (document.querySelector(".profilepic").src !== loggedInUser.photo.split("views\\")[1]) {
        formData.append("photo", userProfilePicFile);
    }

    axios({
        method: 'put',
        url: '/api/user/' + loggedInUser._id,
        data: formData,
        headers: {
            'Content-Type': 'multipart/form-data'
        },
    }).then(response => {
        console.log(response.data);
        setTimeout(() => window.location.reload(), 1000);
    }).catch(error => {
        if (error.response.status === 400) alert("User with the given email already exists!");
        if (error.response.status === 500) alert("Unexpecter error occured! Check console for more details!");
        console.log(error);
        document.getElementById('js-preloader').classList.add('loaded');
    });
});

bookAppointment.addEventListener("click", async (e) => {
    e.preventDefault();
    let specialization = specializationSelected.value.trim();
    let doctor = doctorSelected.value.trim();
    let date = dateSelected.value;
    let time = timeSelected.value;
    if (!specialization || !doctor || !date || !time) return alert("Please fill all the fields before proceeding!");

    let data = {
        user: loggedInUser._id,
        doctor: doctor,
        dateTime: time,
    }

    document.getElementById('js-preloader').classList.remove('loaded');
    axios({
        method: 'post',
        url: '/api/appointment',
        data
    }).then(async (response) => {
        console.log(response.data.data._id);
        let newAppointment = response.data.data._id;
        await axios.put(`/api/user/addAppointment/${doctor}/${newAppointment}`);
        await axios.put(`/api/user/addAppointment/${loggedInUser._id}/${newAppointment}`);
        alert("Appointment made successfully!");
        window.location.reload();
    }).catch(error => {
        console.log(error);
        if (error.response.status === 400) alert("Appointment has already been taken!");
        if (error.response.status === 500) alert("Unexpecter error occured! Check console for more details!");
        document.getElementById('js-preloader').classList.add('loaded');
    });
});

const fetchAndUpdateSlots = async () => {
    const date = appointmentDate.value;
    const doctorId = doctorSelected.value.trim();
    if (!date || !doctorId) return; // Ensure both date and doctor are selected
    document.getElementById('js-preloader').classList.remove('loaded');
    try {
        const slotsResponse = await axios.get(`/api/appointment/available-slots/${doctorId}/${date}`);
        const availableSlots = slotsResponse.data;

        timeSlots.innerHTML = "";
        availableSlots.forEach(slot => {
            const option = document.createElement('option');
            option.value = slot;
            option.textContent = convertISOToReadable(slot);
            timeSlots.appendChild(option);
        });
    } catch (error) {
        console.error("Error fetching available slots:", error);
        alert("Failed to fetch available slots.");
    } finally {
        document.getElementById('js-preloader').classList.add('loaded');
    }
};

specializationSelected.addEventListener("change", async () => {
    document.getElementById('js-preloader').classList.remove('loaded');
    let doctorsSelection = doctors.filter(doctor => doctor.specialization === specializationSelected.value);
    const doctorSelect = document.getElementById("doctor");
    doctorSelect.innerHTML = "";
    doctorsSelection.forEach(doctor => {
        const option = document.createElement('option');
        option.value = doctor._id;
        option.textContent = doctor.name;
        doctorSelect.appendChild(option);
    });
    document.getElementById('js-preloader').classList.add('loaded');
});

appointmentDate.addEventListener("change", fetchAndUpdateSlots);

doctorSelected.addEventListener("change", fetchAndUpdateSlots);

doctorSelected.addEventListener("change", async () => {
    const doctorId = doctorSelected.value;
    let doctorRes = await axios.get(`/api/user/${doctorId}`);
    let doctor = doctorRes.data.data;
    specializationSelected.value = doctor.specialization;
});

function escapeHTML(text) {
    const map = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#039;'
    };
    return text.replace(/[&<>"']/g, function (m) { return map[m]; });
}