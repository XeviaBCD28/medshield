const button = document.querySelector(".btn");

button.addEventListener("click", (e) => {
    e.preventDefault();
    let name = document.getElementById("fullname").value;
    let email = document.getElementById("email").value;
    let password = document.getElementById("password").value;
    let confirmpass = document.getElementById("confirmpassword").value;
    let captchaResponse = grecaptcha.getResponse();

    const validEmailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;
    const strongPasswordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;

    if (!name || !email || !password || !confirmpass) return alert("Please fill all the fields before proceeding!");
    if (!validEmailRegex.test(email)) return alert("Please provide a valid email address!");
    if (!strongPasswordRegex.test(password)) return alert("Please provide a strong password! The password must container atleast one lowercase letter, one uppercase letter, one digit, one special character and a minimum of 8 characters.");
    if (password != confirmpass) return alert("Passwords do not match!");
    if (!captchaResponse.length > 0) return alert("Captcha not complete!");

    document.getElementById('js-preloader').classList.remove('loaded');
    axios({
        method: 'post',
        url: '/api/user/signup',
        data: {
            name,
            email,
            password
        }
    }).then(response => {
        console.log(response.data);
        alert("Account successfully created!");
        window.location.href = '/login';
    }).catch(error => {
        if (error.response.status === 400) alert("User with the given email already exists!");
        if (error.response.status === 500) alert("Unexpecter error occured! Check console for more details!");
        console.log(error);
        document.getElementById('js-preloader').classList.add('loaded');
    });
})