const btn = document.querySelector(".mybtn");
const fullname = document.getElementById("fullname");
const userEmail = document.getElementById("email");
const userPic = document.querySelector(".profilepic");
const appointmentDate = document.getElementById("appointmentDate");
const tbody = document.querySelector(".tablebody");

let loggedInUser;
let doctorAppointments = [];
let patients = [];

window.addEventListener('load', async function () {
    try {
        // Fetch user data
        let userResponse = await axios.get('/api/user/getData');
        let user = userResponse.data;
        loggedInUser = user;
        fullname.placeholder = escapeHTML(user.name);
        userEmail.placeholder = escapeHTML(user.email);
        userPic.src = escapeHTML(user.photo.split("views\\")[1]);

        tbody.innerHTML = "";
        user.appointments.forEach(async (appointmentId) => {
            let appointmentRes = await axios.get(`/api/appointment/${appointmentId}`);
            let appointment = appointmentRes.data.data;
            doctorAppointments.push(appointment);
            let patientRes = await axios.get(`/api/user/${appointment.user}`);
            let patient = patientRes.data.data;
            patients.push(patient);
            let { date, time } = convertISOToDateTime(appointment.dateTime);
            const row = document.createElement('tr');
            row.id = appointmentId;
            const patientTd = document.createElement("td");
            patientTd.textContent = escapeHTML(patient.name);
            const dateTd = document.createElement("td");
            dateTd.textContent = escapeHTML(date);
            const timeTd = document.createElement("td");
            timeTd.textContent = escapeHTML(time);
            const statusTd = document.createElement("td");
            statusTd.textContent = escapeHTML(appointment.status.charAt(0).toUpperCase() + appointment.status.slice(1));
            const actionTd = document.createElement("td");
            actionTd.style.display = "flex";
            actionTd.style.gap = "1rem";
            actionTd.innerHTML = appointment.status === "pending" ? `
                <button class="completeButton actionbtn">Complete</button>
                <button class="cancelButton actionbtn">Cancel</button>
            ` : "";
            row.appendChild(patientTd);
            row.appendChild(dateTd);
            row.appendChild(timeTd);
            row.appendChild(statusTd);
            row.appendChild(actionTd);
            tbody.appendChild(row);
        })
        document.getElementById('js-preloader').classList.add('loaded');
    } catch (error) {
        console.error(error);
        alert("An error occurred while fetching data.");
        document.getElementById('js-preloader').classList.add('loaded');
    }
});


btn.addEventListener("click", (e) => {
    e.preventDefault();
    let name = fullname.value;
    let email = userEmail.value;
    let password = document.getElementById("password").value;
    let confirmpass = document.getElementById("confirmpassword").value;
    const validEmailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;
    const strongPasswordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;

    if (!name || !email || !password || !confirmpass) return alert("Please fill all the fields before proceeding!");
    if (!validEmailRegex.test(email)) return alert("Please provide a valid email address!");
    if (!strongPasswordRegex.test(password)) return alert("Please provide a strong password! The password must container atleast one lowercase letter, one uppercase letter, one digit, one special character and a minimum of 8 characters.");
    if (password != confirmpass) return alert("Passwords do not match!");

    document.getElementById('js-preloader').classList.remove('loaded');

    const formData = new FormData();
    formData.append("name", name);
    formData.append("password", password);
    if (email.toLowerCase() !== loggedInUser.email) {
        formData.append("email", email);
    }
    if (document.querySelector(".profilepic").src !== loggedInUser.photo.split("views\\")[1]) {
        formData.append("photo", userProfilePicFile);
    }

    axios({
        method: 'put',
        url: '/api/user/' + loggedInUser._id,
        data: formData,
        headers: {
            'Content-Type': 'multipart/form-data'
        },
    }).then(response => {
        console.log(response.data);
        setTimeout(() => window.location.reload(), 1000);
    }).catch(error => {
        if (error.response.status === 400) alert("User with the given email already exists!");
        if (error.response.status === 500) alert("Unexpecter error occured! Check console for more details!");
        console.log(error);
        document.getElementById('js-preloader').classList.add('loaded');
    });
})

function convertISOToDateTime(isoString) {
    const date = new Date(isoString);
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0'); // Months are zero-indexed
    const day = String(date.getDate()).padStart(2, '0');
    const dateString = `${year}-${month}-${day}`;
    let hours = date.getHours();
    const minutes = String(date.getMinutes()).padStart(2, '0');
    const seconds = String(date.getSeconds()).padStart(2, '0');
    const ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // The hour '0' should be '12'
    const timeString = `${hours}:${minutes} ${ampm}`;
    return {
        date: dateString,
        time: timeString
    };
}

appointmentDate.addEventListener("change", () => {
    document.getElementById('js-preloader').classList.remove('loaded');
    let appointments = doctorAppointments.filter(appointment => appointment.dateTime.split('T')[0] === appointmentDate.value);
    tbody.innerHTML = "";
    appointments.forEach(appointment => {
        let patient = patients.filter(patient => patient._id === appointment.user);
        let { date, time } = convertISOToDateTime(appointment.dateTime);
        const row = document.createElement('tr');
        row.id = appointment._id;
        const patientTd = document.createElement("td");
        patientTd.textContent = patient[0].name;
        const dateTd = document.createElement("td");
        dateTd.textContent = date;
        const timeTd = document.createElement("td");
        timeTd.textContent = time;
        const statusTd = document.createElement("td");
        statusTd.textContent = appointment.status.charAt(0).toUpperCase() + appointment.status.slice(1);
        const actionTd = document.createElement("td");
        actionTd.style.display = "flex";
        actionTd.style.gap = "1rem";
        actionTd.innerHTML = appointment.status === "pending" ? `
            <button class="completeButton actionbtn">Complete</button>
            <button class="cancelButton actionbtn">Cancel</button>
        ` : "";
        row.appendChild(patientTd);
        row.appendChild(dateTd);
        row.appendChild(timeTd);
        row.appendChild(statusTd);
        row.appendChild(actionTd);
        tbody.appendChild(row);
    })
    document.getElementById('js-preloader').classList.add('loaded');
});

tbody.addEventListener("click", async (e) => {
    if (e.target.classList.contains("completeButton")) {
        await completeAppointment(e);
    } else if (e.target.classList.contains("cancelButton")) {
        await cancelAppointment(e);
    }
});

async function completeAppointment(e) {
    e.preventDefault();
    try {
        let appointmentId = e.target.parentElement.parentElement.id;
        await axios.put(`/api/appointment/changeStatus/${appointmentId}`, { status: "completed" });
        alert("Appointment marked as completed successfully!");
        window.location.reload();
    } catch (error) {
        alert("Something went wrong. Check console for more details.");
        console.log(error);
    }
}

async function cancelAppointment(e) {
    e.preventDefault();
    try {
        let appointmentId = e.target.parentElement.parentElement.id;
        await axios.put(`/api/appointment/changeStatus/${appointmentId}`, { status: "cancelled" });
        alert("Appointment marked as cancelled successfully!");
        window.location.reload();
    } catch (error) {
        alert("Something went wrong. Check console for more details.");
        console.log(error);
    }
}

function escapeHTML(text) {
    const map = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#039;'
    };
    return text.replace(/[&<>"']/g, function (m) { return map[m]; });
}