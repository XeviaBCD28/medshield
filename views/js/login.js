const button = document.querySelector(".btn");
const otpbutton = document.querySelector(".otpbtn");
const closebutton = document.querySelector(".close");
const otpform = document.querySelector(".otpform");
const otpInput = document.getElementById('otp');
let user;

button.addEventListener("click", (e) => {
    e.preventDefault();
    let email = document.getElementById("email").value;
    let password = document.getElementById("password").value;
    let captchaResponse = grecaptcha.getResponse();

    const validEmailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;

    if (!email || !password) return alert("Please fill up both fields before proceeding!");
    if (!validEmailRegex.test(email)) return alert("Please provide a valid email address!");
    if (!captchaResponse.length > 0) return alert("Captcha not complete!");

    document.getElementById('js-preloader').classList.remove('loaded');
    axios({
        method: 'post',
        url: '/api/user/login',
        data: {
            email,
            password
        }
    }).then(response => {
        user = response.data.user;
        document.getElementById('js-preloader').classList.add('loaded');
        if (user.role === "admin") {
            return window.location.href = '/adminpage'
        } else {
            otpform.style.display = "flex";
        }
        // if (response.data.user.role === "admin") return window.location.href = '/adminpage';
        // if (response.data.user.role === "doctor") return window.location.href = '/doctorpage';
        // window.location.href = '/userpage';
    }).catch(error => {
        if (error.response.status === 404) alert("User with the specified email not registered!");
        if (error.response.status === 401) alert("Incorrect password!");
        if (error.response.status === 403) alert("User deactivated!");
        if (error.response.status === 500) alert("Unexpecter error occured! Check console for more details!");
        console.log(error);
        document.getElementById('js-preloader').classList.add('loaded');
    });
});

closebutton.addEventListener("click", () => { otpform.style.display = "none"; });
otpbutton.addEventListener("click", async (e) => {
    e.preventDefault();
    const otpValue = otpInput.value;
    if (otpValue === "") return alert("Enter OTP to verify!");
    document.getElementById('js-preloader').classList.remove('loaded');
    axios({
        method: 'post',
        url: '/api/user/verifyOTP',
        data: {
            user: user._id,
            otp: otpValue
        }
    }).then(response => {
        console.log(response);
        if (user.role === "admin") return window.location.href = '/adminpage';
        if (user.role === "doctor") return window.location.href = '/doctorpage';
        window.location.href = '/userpage';
    }).catch(error => {
        if (error.response.data.error === "Invalid OTP. Check your email!") alert("Invalid OTP. Check your email!");
        console.log(error);
        document.getElementById('js-preloader').classList.add('loaded');
    });
});

document.getElementById('otp').addEventListener('input', function (e) {
    // Replace any non-digit characters with an empty string
    e.target.value = e.target.value.replace(/[^0-9]/g, '');
});