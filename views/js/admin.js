window.addEventListener("load", function () {
    let usersData;
    axios({
        method: "get",
        url: "/api/user/",
    }).then(response => {
        usersData = response.data.data;
        populateTable(usersData);
        attachEventListeners();
    }).catch(error => {
        console.log(error);
        alert("Unable to fetch users data!");
    }).finally(() => document.getElementById("js-preloader").classList.add("loaded"));

    function attachEventListeners() {
        document.getElementById("search").addEventListener("input", filterUsers);
        document.getElementById("selectrole").addEventListener("change", filterUsers);
        document.querySelector(".addDoctor").addEventListener("click", (e) => {
            e.preventDefault();
            document.querySelector(".formContainer").style.visibility = "visible";
        });
        document.querySelector(".close").addEventListener("click", (e) => {
            e.preventDefault();
            document.querySelector(".formContainer").style.visibility = "hidden";
        });
        document.querySelector(".addbtn").addEventListener("click", (e) => {
            e.preventDefault();
            let name = document.getElementById("fullname").value;
            let email = document.getElementById("email").value;
            let password = generateSecurePassword();
            let specialization = document.getElementById('specialization').value;
            const validEmailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;
            if (!name || !email) return alert("Please fill all the fields before proceeding!");
            if (!validEmailRegex.test(email)) return alert("Please provide a valid email address!");

            document.getElementById('js-preloader').classList.remove('loaded');
            axios({
                method: 'post',
                url: '/api/user/signup',
                data: {
                    name: name,
                    email: email,
                    password: password,
                    role: "doctor",
                    specialization: specialization
                }
            }).then(response => {
                console.log(response.data);
                alert(`Account created successfully!`)
                window.location.reload();
            }).catch(error => {
                if (error.response.status === 400) alert("User with the given email already exists!");
                if (error.response.status === 500) alert("Unexpecter error occured! Check console for more details!");
                console.log(error);
                document.getElementById('js-preloader').classList.add('loaded');
            });
        })


        if (document.querySelector(".deactivate")) {
            document.querySelectorAll(".deactivate").forEach(btn =>
                btn.addEventListener("click", deactivateUser)
            );
        }
        if (document.querySelector(".activate")) {
            document.querySelectorAll(".activate").forEach(btn =>
                btn.addEventListener("click", activateUser)
            );
        }
    }

    function filterUsers() {
        const searchQuery = document.getElementById("search").value.toLowerCase();
        const selectedRole = document.getElementById("selectrole").value;
        const filteredUsers = usersData.filter(user => {
            const nameMatches = user.name.toLowerCase().includes(searchQuery);
            const emailMatches = user.email.toLowerCase().includes(searchQuery);
            const roleMatches = selectedRole === "All" || user.role.toLowerCase() === selectedRole.toLowerCase();
            return (nameMatches || emailMatches) && roleMatches;
        });
        populateTable(filteredUsers);
    }

    function populateTable(users) {
        const tbody = document.querySelector(".tablebody");
        tbody.innerHTML = "";
        users.forEach(user => {
            const row = document.createElement("tr");
            row.id = user._id;
            const nameTd = document.createElement("td");
            nameTd.textContent = user.name;
            const emailTd = document.createElement("td");
            emailTd.textContent = user.email;
            const roleTd = document.createElement("td");
            roleTd.textContent = user.role.charAt(0).toUpperCase() + user.role.slice(1);
            const statusTd = document.createElement("td");
            statusTd.textContent = user.is_active ? "Active" : "Not Active";
            const actionTd = document.createElement("td");
            const actionButton = document.createElement("button");
            actionButton.classList.add("actionbtn");
            actionButton.classList.add(user.is_active ? "deactivate" : "activate");
            actionButton.textContent = user.is_active ? "Deactivate" : "Activate";
            actionTd.appendChild(actionButton);
            row.appendChild(nameTd);
            row.appendChild(emailTd);
            row.appendChild(roleTd);
            row.appendChild(statusTd);
            row.appendChild(actionTd);
            tbody.appendChild(row);
        });
    }

    function generateSecurePassword(length = 12) {
        const uppercaseCharset = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        const lowercaseCharset = "abcdefghijklmnopqrstuvwxyz";
        const numberCharset = "0123456789";
        const specialCharset = "!@#$%^&*()_+~`|}{[]:;?><,./-=";
        let password = '';
        password += uppercaseCharset[Math.floor(Math.random() * uppercaseCharset.length)];
        password += lowercaseCharset[Math.floor(Math.random() * lowercaseCharset.length)];
        password += numberCharset[Math.floor(Math.random() * numberCharset.length)];
        password += specialCharset[Math.floor(Math.random() * specialCharset.length)];
        const allCharsets = uppercaseCharset + lowercaseCharset + numberCharset + specialCharset;
        for (let i = password.length; i < length; i++) {
            password += allCharsets[Math.floor(Math.random() * allCharsets.length)];
        }
        password = password.split('').sort(() => 0.5 - Math.random()).join('');
        return password;
    }


    async function deactivateUser(e) {
        e.preventDefault();
        try {
            let userId = e.target.parentElement.parentElement.id;
            await axios.put(`/api/user/changeStatus/${userId}`, { is_active: false });
            alert("User deactivated successfully!");
            window.location.reload();
        } catch (error) {
            alert("Something went wrong. Check console for more details.");
            console.log(error);
        }
    }

    async function activateUser(e) {
        e.preventDefault();
        try {
            let userId = e.target.parentElement.parentElement.id;
            await axios.put(`/api/user/changeStatus/${userId}`, { is_active: true });
            alert("User activated successfully!");
            window.location.reload();
        } catch (error) {
            alert("Something went wrong. Check console for more details.");
            console.log(error);
        }
    }
});